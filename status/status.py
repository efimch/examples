# Модуль статусов различных сущьностей в системе.

from .utils import rupluralize


class Status:
    """
    Статусы для Анонсов. Определяет интерфейс и логику работы.
    """

    def __init__(self, name=None, text=None, css_color=None, words=None):
        super(Status, self).__init__()
        self.name = name
        self._text = text
        self._words = words
        self.css_color = css_color

    def text(self):
        return self._text

    def format_text(self, value):
        self._text = self._text.format(value, rupluralize(value, self._words))
        return self


class DaysStatus(Status):
    """
    Статусы принимающие два набора слов.
    """

    def format_text(self, value):
        self._text = self._text.format(rupluralize(
            value, self._words[0]), value, rupluralize(value, self._words[1]))
        return self


class StatusHandle:
    """
    Переопределяемый набор статусов.
    """

    def __init__(self):
        self.OPEN = Status(
            name='OPEN', text='Регистрация открыта', css_color='green')
        self.CLOSE = Status(
            name='CLOSE', text='Регистрация закрыта', css_color='gray')
        self.WAIT_LIST = Status(
            name='WAIT_LIST', text='Регистрация в лист ожидания', css_color='gray')
        self.PLACE = Status(
            name='PLACE', text='Осталось {} {}!', css_color='', words="место,места,мест")
        self.DAYS = DaysStatus(
            name='DAYS', text='{} {} {} до начала!', css_color='',
            words=("Остался,Осталось,Осталось", "день,дня,дней"))
        self.RUN = Status(
            name='RUN', text='Курс идет', css_color='gray')
        self.FINISH = Status(
            name='FINISH', text='Курс закончился', css_color='gray')


class EventStatusHandle(StatusHandle):
    """
    Статусы для событий
    """

    def __init__(self):
        super().__init__()
        self.RUN = Status(name='RUN', text='Событие идет', css_color='gray')
        self.FINISH = Status(name='FINISH', text='Событие закончилось', css_color='gray')


class CompetitionStatusHandle(StatusHandle):
    """
    Статусы для соревнований
    """

    def __init__(self):
        super().__init__()
        self.RUN = Status(name='RUN', text='Соревнование идет', css_color='gray')
        self.FINISH = Status(name='FINISH', text='Соревнование закончилось', css_color='gray')
