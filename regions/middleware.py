from django.utils.deprecation import MiddlewareMixin
from .region import get_location, get_region, get_user_region, get_default_region, get_session_region


class RegionMiddleware(MiddlewareMixin):

    def process_request(self, request):
        request.region = get_session_region(request) or get_user_region(
            request) or get_region(get_location(request)) or get_default_region()
