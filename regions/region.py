# Простой модуль определения региональности пользователя

from django.contrib.gis.geoip2 import GeoIP2
from django.db.models import Q
from django.contrib.auth import get_user_model

from .models import Region

User = get_user_model()


def get_location(request):
    g = GeoIP2()
    REMOTE_ADDR = request.META.get('HTTP_X_REAL_IP')
    try:
        city = g.city(REMOTE_ADDR)
        return city
    except Exception:
        pass


def get_session_region(request):
    if request.session.get('region', False):
        return Region.objects.get(pk=request.session['region'])


def get_user_region(request):
    if request.user.is_authenticated:
        return request.user.region


def get_default_region():
    """
    Регион по умолчанию записывается к анонимному юзеру.
    Это нужно указать в админке в ручную.
    """
    return User.objects.get(username='AnonymousUser').region


def get_region(location):
    if location:
        region = Region.objects.filter(
            Q(continent_code=location['continent_code']) |
            Q(country_code=location['country_code']) |
            Q(time_zone=location['time_zone']) |
            Q(region=location['region']) |
            Q(city=location['city'])
        )
        if region:
            return region.first()
